<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SenderMailRichieste extends CI_Controller {
    private $durataRichiesta="10"; //validità richiesta in giorni
    private $mailSended=0; //numero di mail inviate
    private $limitMailSend=20; //limite di invio mail per richiesta
    private $limiteRichiesteComprate=5;


    public function index(){
        //niente da fare qui
    }

    public function sendMail()
    {
        $this->load->library('email');
        $subject = 'This is a test';
        $message = '<p>This message has been sent for testing purposes.</p>';
        // Get full html:
        $body = 'INVIO MAIL';
        // Also, for getting full html you may use the following internal method:
        //$body = $this->email->full_html($subject, $message);

        $result = $this->email
        ->from('yourusername@gmail.com')
        ->reply_to('yoursecondemail@somedomain.com')    // Optional, an account where a human being reads.
        ->to('therecipient@otherdomain.com')
        ->subject($subject)
        ->message($body)
        ->send();
    }




    /**CARICAMENTO DATI DA PROCESSARE **/

    public function start(){
        $this->load->database();
        $durata=$this->durataRichiesta;
        $limiteComprate=$this->limiteRichiesteComprate;
        $str="SELECT richiesta.idRichiesta,telefono,mail,nota,richiesta.cognome,richiesta.nome,categoria,
        categoria.descrizione as descrizioneCategoria,provincia,comune,count(richiesta.idRichiesta) as comprate
        from richiesta
        left join richiesta_invio as invio on invio.idRichiesta=richiesta.idRichiesta and invio.stato=2
        left join richiesta_categoria as categoria on categoria.idCategoria=richiesta.categoria
        where richiesta.stato=1 and date_add(dataInserimento, interval $durata DAY)>=now()
        group by richiesta.idRichiesta having comprate<=$limiteComprate  ";
        $res=$this->db->query($str);
        $this->procRichiesta($res);
    }

    /**
    * Aziende attive in questo periodo?
    * @param  dataset $richiesta da cui partire per estrarre le aziende
    * @return dataset
    */
    private function getAziendeRichiesta($richiesta){
        $idRichiesta=$richiesta['idRichiesta'];
        $categoria=$richiesta['categoria'];
        // $idComune=$richiesta['comune'];
        $idProvincia=$richiesta['provincia'];

        $str=" SELECT azienda.idAzienda,azienda.mail,azienda.ragioneSociale from azienda
        inner join azienda_ricerca as ric on ric.idAzienda=azienda.idAzienda and ric.dataInizio<=now()
        and ( ric.dataFine is null or ric.dataFine>=now())
        and ric.idCategoria=? and  idProvincia=?
        left join richiesta_invio as invio on invio.idAzienda=azienda.idAzienda and invio.idRichiesta=?
        where azienda.stato=1  and invio.idAzienda is null ";
        $res=$this->db->query($str,array($categoria,$idProvincia,$idRichiesta));
        return $res;
    }

    /**
     * Converte la zona in stringa umana
     * @param  [type] $idComune    [description]
     * @param  [type] $idProvincia [description]
     * @return [type]              [description]
     */
    private function getZona($idComune,$idProvincia){
        $res=$this->db->query(" SELECT nomeComune from zona_comune where idComune=? ",array($idComune));
        $comune=$res->result()->nomeComune;
        $res=$this->db->query(" SELECT nomeProvincia from zona_provincia where idProvincia=? ",array($idProvincia));
        $provincia=$res->result()->nomeProvincia;
        return "$comune in provincia di $provincia";
    }


    /**
    * Genera e torna un otp valido per la richiesta
    * @param  string $idRichiestaInvio [description]
    * @return string                   [description]
    */
    private function getOtp(string $idRichiestaInvio,string $idAzienda):string{

        $char = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charLength = strlen($char);
        $randomString = '';
        for ($i = 0; $i < $charLength; $i++) {
            $randomString .= $char[rand(0, $charLength - 1)];
        }
        $codice = $randomString."-".$idRichiestaInvio."-".$idAzienda;
        return $codice;
    }


    /**
    * Invia la mail e signa l'invio in database
    * @param  datasetRow $azienda
    * @param  datasetRow $richiesta
    * @return bool       eisto operazione
    */
    private function sendMailAndRecord($azienda,$richiesta):bool{
        $idRichiesta=$richiesta['idRichiesta'];
        $idAzienda=$azienda['idAzienda'];
        $token=$this->getOtp($idRichiesta,$idAzienda);

        $idRichiesta=$idRichiesta;
        /**
        * Inserisco la richiesta a db
        */
        $str="INSERT INTO richiesta_invio(idAzienda,idRichiesta,dataInvio,stato,token)
        values('$idAzienda','$idRichiesta',now(),0,'$token')";
        $res=$this->db->query($str);
        $idRichiestaInvio=$this->db->insert_id();

        /** CARICAMENTO DELLA MAIL**/
        $categoria=$richiesta['descrizioneCategoria'];
        $idComune=$richiesta['comune'];
        $idProvincia=$richiesta['provincia'];
        $zona=$this->getZona($idComune,$idProvincia);
        $mailAzienda=$azienda['mail'];
        $data['ragione']=$azienda['ragioneSociale'];

        $this->load->library('email');
        $esito=$this->email
        ->to($mailAzienda)
        ->subject("Nuova richiesta nella zona $zona - categoria $categoria da soluzioni edili 24")
        ->message($this->getBodyMail($zona,$categoria,$token))
        ->send();


        if($esito){
            $this->limitMailSend++; //CONTATORE MAIL PER L'INVIO FINO AD ESAURIMENTO CODA o MASSIME MAIL
            $this->db->query("UPDATE richiesta_invio set stato=1 where idRichiestaInvio=? ",array($idRichiestaInvio)); //MAIL INVIATA CON SUCCESSO
            return true;
        }else{
            return false;
        }

    }//fine procedura invio mail

    /**
    * cicla le aziende trovate
    * @param  [type] $installatori [description]
    * @param  [type] $richiesta    [description]
    * @return [type]               [description]
    */
    private function procAziende($aziende,$richiesta){
        foreach($aziende->result_array() as $azienda){
            $this->sendMailAndRecord($azienda,$richiesta);
        }
    }

    /**
    * Ciclo della singola richiesta
    * @param  [type] $res dataset richiesta
    * @return [type]      [description]
    */
    private function procRichiesta($res){
        foreach ($res->result_array() as $row)
        {
            $aziende=$this->getAziendeRichiesta($row);
            $this->procAziende($aziende,$row);

        }
    }

    /**
    * Genera il testo della mail
    * @param  [type] $zona      [description]
    * @param  [type] $categoria [description]
    * @param  [type] $token     [description]
    * @return [type]            [description]
    */
    private function getBodyMail($zona,$categoria,$token){
        ob_start();
        ?>
        Gentile utente,<br>
        È arrivata una nuova richiesta in zona <?= $zona ?> da un cliente che richiede servizi di tipo <?= $categoria ?><br>
        <br>
        Per acquistare il contatto apri il link sottostante <br>
        <br>
        <a href="<?=$token ?>">LINK PER L'ACQUISTO DEL NOMINATIVO</a>
        <br><br>
        SOLUZONI EDILI 24
        <?php
        $text=ob_get_contents();
        ob_end_clean();
        return $text;
    }

}
