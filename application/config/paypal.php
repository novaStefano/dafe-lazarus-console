<?php
/** set your paypal credential **/

$config['client_id'] = 'AaYH7nKyK6BgEqghjlgjzZ_CdMx7_m4eMsv0HnmleGK8lS-df_ffGHGPDABwiBVdJHUYlcjEgtb_9RJI';
$config['secret'] = 'EOODeanG1Zu3AD4FHrfdmCqo9LygT_6DckdpH61UyemzXRHslP9UzCCukGqv1DtkuKlW_bRVfBYku14t';

/**
 * SDK configuration
 */
/**
 * Available option 'sandbox' or 'live'
 */
$config['settings'] = array(

    'mode' => 'sandbox',
    /**
     * Specify the max request time in seconds
     */
    'http.ConnectionTimeOut' => 1000,
    /**
     * Whether want to log to a file
     */
    'log.LogEnabled' => true,
    /**
     * Specify the file that want to write on
     */
    'log.FileName' => 'application/logs/paypal.log',
    /**
     * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
     *
     * Logging is most verbose in the 'FINE' level and decreases as you
     * proceed towards ERROR
     */
    'log.LogLevel' => 'FINE'
);
