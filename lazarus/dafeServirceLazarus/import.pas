unit import;

{$mode objfpc}{$H+}

interface

uses
  Classes;

type

  { TimpOld }

  TimpOld = class(TClass)
    logm: TMemo;
    err: TMemo;
    k: TMemo;
    main: TProgressBar;
    Panel1: TPanel;
    setIns: TZQuery;
    setDir: TZQuery;
    setRef: TZQuery;
    setApp: TZQuery;
    procedure addOk(mess:string);
    procedure addErr(mess:string);
    procedure log(mess:string);
    procedure checkDirRoot(dir:string);
    function creaCartella():string;
    function creaCartellaSub(idPadre,nome:string):string;
    procedure checkDIr(dir,id:string);
    function insFile(nome,id:string):boolean;
    function getIdFolderParent(idUtente,idParent,cartella:string):string;
    procedure savFile();

    procedure loadImportazione(idImp:string);
    function checkDirImp(path:string):string;
    function getCreaIdFolderImp(idUte,path,idPre:string):string;
   function  getIdFolderImp(idUte:string):string;
   procedure checkFileImp(idUte,cartella,idFolder:string);
  private
    { private declarations }
  public
   insSottoValore:string; //gestione delle sotto azienda
    { public declarations }
  end;

var
  impOld: TimpOld;

implementation

{$R *.lfm}

{ TimpOld }

uses menu;



function timpOld.creaCartella():string;
var percorso,ragione,idUtente:string;
begin
ragione:=setDir.fieldbyname('ragioneSociale').asstring;
            idUtente:=setDir.fieldbyname('idUtente').asstring;
         percorso:=(ragione)+'-'+idUtente+'~';
         percorso:=inizio.removeCar(percorso);


     inizio.Query(setApp,' select idCartella from cartella where idUtente="'+idUtente+'" and cartellaPadre is null ');   //cerco la cartella root dell'utente
     if setApp.RecordCount=1 then
        begin
          creaCartella:=setApp.fields[0].asstring;
          exit;
        end;

                 inizio.Query(setIns,' insert ignore cartella (nome,percorso,idUtente,dataInserimento) '+
                 ' values("'+ragione+' '+idUtente+'","'+inizio.api(percorso)+'",'+idUtente+',now()) ');

                 try
                 mkdir(inizio.path+ StringReplace(percorso,'~',DirectorySeparator,[]));
                 except
                       on e:exception do
                          begin
                          showmessage(e.Message);
                           inputbox('','',inizio.path+ StringReplace(percorso,'~',DirectorySeparator,[]));

                          end;
                 end;

     setApp.Refresh;
     creaCartella:=setApp.fields[0].asstring;

end;


function timpOld.creaCartellaSub(idPadre,nome:string):string;
var percorso,idUtente:string;
begin
     log('Ricerca cartella per ID:'+nome);
     idUtente:=setDir.fieldbyname('idUtente').asstring;
     inizio.Query(setApp,' select percorso from cartella where idUtente="'+idUtente+'" and idCartella="'+idPadre+'" ');   //cerco la cartella root dell'utente
     percorso:=(setApp.FieldByName('percorso').asstring+nome)+'~';

     percorso:=inizio.removeCar(percorso);

     log('Percorso '+percorso);


     inizio.Query(setApp,' select idCartella from cartella where idUtente="'+idUtente+'" and percorso="'+inizio.api(percorso)+'" ');   //cerco la cartella root dell'utente

     if setApp.RecordCount=1 then
        begin
          creaCartellaSub:=setApp.fields[0].asstring;
          exit;
        end;

                 inizio.Query(setIns,' insert ignore cartella (nome,percorso,idUtente,dataInserimento,cartellaPAdre) '+
                 ' values("'+nome+'","'+inizio.api(percorso)+'",'+idUtente+',now(),"'+idPadre+'" ) ');

                 try
                 if DirectoryExists(inizio.path+ inizio.convDir(percorso))=false then
                 mkdir(inizio.path+ inizio.convDir(percorso));
                 except
                       on e:exception do
                          begin
                          showmessage(e.Message);

                          end;
                 end;

     setApp.Refresh;
     creaCartellaSub:=setApp.fields[0].asstring;

end;



//inserisce un file processato
function timpOld.insFile(nome,id:string):boolean;
var appFile,newFile,strQuery:string;
  size:Integer;
begin

     // nome:=inizio.removeCar(nome);
     log('Trovato file '+nome);


     appFile:=(ExtractFileName(nome));
     size:=FileSize(nome);

          if(insSottoValore<>'') then  //gestisco la sotto azienda
                            begin
                              appFile:=ExtractFileNameWithoutExt(appFile);
                              appFile:=appFile+'-'+insSottoValore+ ExtractFileExt(nome);
                            end;


     strQuery:= ' insert ignore file(nome,idCartella,dataInserimento,size)values("'+inizio.api(appFile)+'","'+id+'",now(),"'+IntToStr(size)+'") ';
     inizio.Query(setApp,strQuery);

     inizio.Query(setApp,' select percorso from cartella where idCartella="'+id+'" ');
     newFile:=inizio.path+inizio.convDir(setApp.Fields[0].asstring)+appFile;

     try
     if FileExists(newFile) then
        begin
             log('Elimino il file vecchio');
             try
             DeleteFile(newFile);

             Except on e:Exception do
              log('Errore eliminazione vecchio file '+e.Message);

             end;

        end;

     if CopyFile(nome,newFile ,false) then
        begin
            log('copia file avvenuto con successo');
            log(newFile);
             insFile:=true;
             exit;

        end
        else
            begin
                 addErr('*****************************Errore file!');
                 log('*****************************Errore file!');
            end;



     except on e:exception do
            begin
           addOk('!!!!!!!!!!!!!!!!!!!!!!!!!!!errore copia file '+e.message+'  '+nome+'   '+newFile);
           log('!!!!!!!!!!!!!!!!!!!!!!!!!!!errore copia file '+e.message+'  '+nome+'   '+newFile);

            end;
     end;
     insFile:=false;

end;



procedure timpOld.checkDIr(dir,id:string);
var fil:TstringLIST;
  i:integer;
  appFile,appId:string;

begin

     log('Inizio processo cartella '+dir );
fil:=TStringList.Create;
fil:=FindAllDirectories(dir,false);
for i:=0 to fil.Count-1 do  //ricorsione per file
    begin
         appId:=creaCartellaSub(id,ExtractFileName(fil.strings[i]));
         checkDir(fil.Strings[i],appId);
    end;


//inizio caricamento file
fil:=FindAllFiles(dir,'*.*',false);
log('inizio ricerca file '+dir+' trovati '+inttostr(fil.count));
for i:=0 to fil.Count-1 do  //ricorsione per file
    begin
         appFile:=(dir+DirectorySeparator+ExtractFileName(fil.strings[i]));
         insFile(appFile,id);
         Application.ProcessMessages;
    end;




end;

procedure timpOld.checkDirRoot(dir:string);
var dirName:string;
    idRoot:string;

    app:string;
    ind:integer;
begin
 dirName:=ExtractFileName(dir);


inizio.Query(setDir,' select * from utente where nota="'+dirName+'" ');
     if setDir.RecordCount=0 then
        begin
             ind:=Pos('_',dirname);
             app:=Copy(dirname,1,ind-1);

             inizio.Query(setDir,' select * from utente where idReferente="'+app+'" ');
        end;

if setDir.RecordCount=0 then
   begin
     addErr('Errore '+dir+' non trovata nel db');
   end
else
    begin
      addOk('Cartella identificata con l''utente '+setDir.fieldbyname('idUtente').asstring+' '+setDir.FieldByName('ragioneSociale').asstring+' ');
      idRoot:=creaCartella();
      checkDir(dir,idRoot);
      Application.ProcessMessages;

    end;


end;

procedure TimpOld.addOk(mess:string);
begin
k.Lines.Add(mess);
end;


procedure TimpOld.log(mess:string);
begin
logm.Lines.Add(mess);
end;

procedure TimpOld.addErr(mess:string);
begin
err.Lines.Add(mess);

end;

procedure TimpOld.Button2Click(Sender: TObject);
var str:string;  ragione,appId:string; percorso:string;
begin

showmessage('inizio creazione cartelle utenti nuovi');
  str:=' select * from utente left join cartella on utente.idUtente=cartella.idUtente '+
  ' and cartella.cartellaPadre is null where cartella.idCartella is null ';
  inizio.Query(setApp,str);

  main.Max:=setApp.RecordCount;
  main.Position:=0;
  while not setApp.EOF do
        begin

                      appId:=setApp.fieldbyname('idUtente').asstring;
                      ragione:=setApp.fieldbyname('ragioneSociale').asstring;
                      percorso:=ragione+'-'+setApp.fieldbyname('idUtente').AsString+'~';
                      inizio.Query(setIns,' insert into cartella (nome,percorso,idUtente,dataInserimento) '+
                      ' values("'+ragione+' '+appId+'","'+percorso+'",'+appId+',now()) ');

                      try
                      mkdir(inizio.path+ StringReplace(percorso,'~',DirectorySeparator,[]));
                      except
                            inputbox('','',inizio.path+ StringReplace(percorso,'~',DirectorySeparator,[]));
                      end;
          setApp.Next;

          main.Position:=main.Position+1;

        end;


end;

procedure TimpOld.Button3Click(Sender: TObject);
begin
savFile();
end;

procedure TimpOld.Button1Click(Sender: TObject);
var cart:string;
fil:TStringList;
i:integer;
start:integer;
appCart:string;
begin
inizio.query(setRef,' select valore from impostazioni where nome="path_old" ');
cart:=setRef.FieldByName('valore').asstring;
cart:=inizio.convDir(cart);
addOk('Caricamento '+cart);

fil:=TStringList.Create;
fil:=FindAllDirectories(cart,false);
addOk('Trovati '+inttostr(fil.Count));
main.Max:=fil.Count;
main.Min:=0;
main.Position:=1;

start:=strtoint(inputbox('','','0'));
for i:=start To fil.Count-1 do
    begin

      addOk('**********Processo '+inttostr(i));
      main.Position:=i;
      checkDirRoot(fil.Strings[i]);
      Application.ProcessMessages;
    end;

end;

function timpOld.checkDirImp(path:string):string;
var rep:string;
appPath:string;
id:string;
begin
     insSottoValore:='';    //variabile globale per il controllo del sotto utente
     rep:=setRef.FieldByName('tracciato').asstring;
     appPath:=ExtractFileName(path);
     id:=StringReplace(appPath,rep,'',[]);
     log('Path processata da '+rep+' '+id);
     try     //controllo la validità dell'indice                                                                     |
     StrToInt(id);
     log('Controllo valido');
     except
           log('id non valido '+id);
           addErr(id+' nel formato non valido ');
           checkDirImp:='';
           Exit;
     end;

     inizio.query(setapp,'select idUtente,ragioneSociale,if(idReferente like "%|%","'+id+'","") as sottoAzienda from utente where idReferente='+id+' or idReferente like "%|'+id+'|%" ');
     log('Query eseguita');
     if setapp.RecordCount=1 then
                             begin

                                  addOk('Contatto trovato '+setApp.FieldByName('idUtente').asstring+' '+setApp.FieldByName('ragioneSociale').asstring+' sotto azienda:'+setApp.FieldByName('sottoAzienda').asstring);
                                  log('Marchio per la notifica mail '+setApp.FieldByName('idUtente').asstring);
                                  inizio.Query(setins,'insert ignore mail_coda(idUtente,stato,tipo) '+
                                  ' values("'+ setApp.FieldByName('idUtente').asstring+'","0","nuovoFile")');
                                  insSottoValore:=setApp.FieldByName('sottoAzienda').asstring;
                                  checkDirImp:= setApp.FieldByName('idUtente').asstring;


                             end
                                else
                                    begin
                                      addErr('Contatto non trovato '+path);
                                      checkDirImp:='';
                                    end;

end;



function timpOld.getCreaIdFolderImp(idUte,path,idPre:string):string;
var app,percorso,newPerc,dir:string;
begin

log('controllo nodo '+path);
if idPre='' then
   begin
        inizio.Query(setapp,'select idCartella,percorso from cartella where idUtente="'+idUte+'" and cartellaPadre is null ');
        idPre:=setApp.fields[0].asstring;
   end
else    inizio.Query(setapp,'select idCartella,percorso from cartella where idUtente="'+idUte+'" and idCartella="'+idPre+'" ');

percorso:=setApp.fieldbyname('percorso').asstring;


   newPerc:=(percorso+path);
   //showmessage(newPerc);
   log('percorso '+newPerc);


   inizio.query(setapp,'select idCartella from cartella where idUtente="'+idUte+'" and percorso="'+inizio.api(newPerc)+'" ');
   if Setapp.recordcount=1 then
      begin  //cartella trovata, esco restituendo l'id
             log('Cartella trovata '+setApp.Fields[0].asstring);
             getCreaIdFolderImp:=setApp.Fields[0].asstring;
             exit;

       end;
   log('Creo la cartella '+path);

  // showmessage('creazione cartella '+newPerc);
   inizio.Query(setIns,' insert ignore cartella (nome,percorso,idUtente,dataInserimento,cartellaPAdre) '+
                 ' values("'+StringReplace(path,'~','',[rfReplaceAll])+'","'+inizio.api(newPerc)+'",'+idUte+',now(),"'+idPre+'" ) ');

                 try
                 dir:=inizio.path+ inizio.convDir(percorso)+inizio.convDir(path);
                 log('Creazione '+dir);
                 if DirectoryExists(dir)=false then
                 mkdir(dir);
                 except
                       on e:exception do
                          begin
                          showmessage(e.Message+'  '+dir);

                          end;
                 end;

     setApp.Refresh;
     getCreaIdFolderImp:=setApp.fields[0].asstring;





end;


//cerca l'id directory dall'idUtente e da una cartella superiore partendo dall'id
function timpOld.getIdFolderParent(idUtente,idParent,cartella:string):string;
var str:string;
percorsoAssoluto:string;
begin

//estraggo il nome della cartella da importare
cartella:=(ExtractFileName(cartella));
log('Cartella da cercare:'+cartella+' id cartella padre: '+idParent );

//cerco se la cartella è già esistente
str:=' select idCartella from cartella where idUtente="'+idUtente+'" and nome="'+inizio.api(cartella)+'" and cartellaPadre="'+idParent+'" ';
inizio.query(setApp,str);


if(setApp.recordcount=1)then
   begin
        getIdFolderParent:=setApp.fields[0].asstring;
   end
      else
          begin    //inserisco il record e restituisco l'id

              inizio.query(setApp,'select percorso from cartella as a where a.idCartella="'+idParent+'" ');
              percorsoAssoluto:=setApp.fieldbyname('percorso').asstring;
              percorsoAssoluto:=percorsoAssoluto+cartella+'~';



            str:=' insert into cartella set idUtente="'+idUtente+'",nome="'+inizio.api(cartella)+'",cartellaPadre="'+idParent+
               '",percorso="'+inizio.api(percorsoAssoluto)+'",dataInserimento=now() ';
               inizio.query(setApp,str);
               log(str);

               //riconverto la tilde per il tracciato corretto per creare la directory
               percorsoAssoluto:=inizio.path+inizio.convDir(percorsoAssoluto);
               log('Creo la directoy '+percorsoAssoluto);
               mkdir(percorsoAssoluto);

               //richiamo la stessa funzione, ora il record c'è
               getIdFolderParent:=getIdFolderParent(idUtente,idParent,cartella);

          end;



end;

//cerco l'id directory e se non c'è la creo
function timpOld.getIdFolderImp(idUte:string):string;
var app:string; idApp,appCart,modCart,root:string;
ind:integer;
i:integer;
begin

     log('inizio ricerca indice cartella '+idUte);


     app:=setRef.FieldByName('tracciatoCartella').asstring;
     inizio.query(setApp,'select '+app);
     log('select '+app);
     modCart:=inizio.api( setApp.fields[0].asstring);
     inizio.Query(setapp,'select percorso from cartella where idUtente="'+idUte+'" and cartellaPadre is null ');
     root:=setApp.Fields[0].asstring;

     app:=root+modCart;
     log('Query -->select idCartella from cartella where idUtente="'+idUte+'" and percorso="'+app+'" ');

     inizio.query(setApp,'select idCartella from cartella where idUtente="'+idUte+'" and percorso="'+app+'"');
     if setApp.recordcount=1 then
                                 begin
                                     log('Cartella trovata restituisco l''id '+setApp.fields[0].asstring);
                                     getIdFolderImp:=setApp.fields[0].asstring;
                                     exit;
                                  end;
     log('Cartella non trovata, creo la cartella e il relativo percorso');
      i:=0;
     while (modCart<>'')and(i<30) do   //uscita di sicurezza a 30
           begin
     ind:=pos('~',modCart);
     appCart:=copy(modCart,1,ind);
     modcart:=copy(modCart,ind+1,length(modCart));
     log('**Stringa: '+modCart+' CREO:'+appCart);
     idApp:=getCreaIdFolderImp(idUte,appCart,idApp);
     log('nodo '+modCart+'  '+idApp);
     inc(i);
           end;


       log('Cartella creata restituisco l''id');
       getIdFolderImp:=idApp;


end;


//ricerca nella cartella i file da importare per l'utente - se Id cartella è vuoto viene cercato seguendo il pattern dell'importazione
procedure timpOld.checkFileImp(idUte,cartella,idFolder:string);
var fil:tstringlist;
i:integer;
idFolderCiclo:string; //utilizato per il ciclo della ricerca cartella
begin
log('inizio ricerca file cartella '+cartella);
fil:=TStringList.Create;
fil:=FindAllFiles(cartella,'*.*',false);
if (idFolder='') then idFolder:=getIdFolderImp(idUte);  //prima entrata, estraggo/creo la cartella root del tracciato
log(idFolder);

//ciclo per controllare la quantità di file
for i:=0  to fil.Count-1 do
    begin
        if insFile(fil.Strings[i],idFolder) then
         begin
             DeleteFileUTF8(fil.strings[i]);
         end
         else log('Annullo l''eliminazione del file');
    end;



log('inizio ciclo directory da importare');
//ciclo la lista di directory, ricorsivamente richiamo questa routine

fil:=FindAllDirectories(cartella,false);
addOk('Trovati '+inttostr(fil.Count));
for i:=0 To fil.Count-1 do
    begin
        cartella:=fil.Strings[i];
        log('Processo sotto cartella '+cartella);
        //processo anche la sotto cartella
        idFolderCiclo:=getIdFolderParent(idUte,idFolder,cartella);
        checkFileImp(idUte,cartella,idFolderCiclo);
    end;



end;

procedure timpOld.loadImportazione(idImp:string);
var cart:string;
fil:tstringList;
i:integer;
idUtente,app:string;
dir:string;
begin
k.Lines.Text:='';
;
logm.Lines.Text:='';
err.Lines.Text:='';


app:=InputBox('Anno','Inserisci l''anno dove inserire i documenti','2015');
inizio.query(setRef,' select idImportazione,cartellaSorgente,domanda, replace(tracciatoCartella,"$sost","'+app+'") as "tracciatoCartella",tracciato '+
' from importazione where idImportazione="'+idImp+'" ');

if inizio.modi(setRef.FieldByName('domanda').asstring)=false then exit;
cart:=setRef.FieldByName('cartellaSorgente').asstring;
cart:=inizio.convDir(cart);
addOk('Caricamento '+cart);

fil:=TStringList.Create;
fil:=FindAllDirectories(cart,false);
addOk('Trovati '+inttostr(fil.Count));
main.Max:=fil.Count;
main.Min:=0;
main.Position:=1;
for i:=0 To fil.Count-1 do
    begin
      idUtente:='';
      addOk('**********Processo '+inttostr(i)+' '+fil.strings[i]);
      log('**********Processo '+inttostr(i)+' '+fil.strings[i]);
      addErr('**********Processo '+inttostr(i)+' '+fil.strings[i]);
      main.Position:=i;
      idUtente:=checkDirImp(fil.Strings[i]);

      if (idUtente<>'') then
         begin
              log('Inizio Importazione utente '+idUtente);
              checkFileImp(idUtente,fil.strings[i],'');


         end;
      Application.ProcessMessages;
    end;




main.Position:=main.Max;

savFile();

showmessage('Ho terminato con successo!');




end;

procedure timpOld.savFile();
var dir:string;
begin
dir:=ExtractFileDir(Application.ExeName)+DirectorySeparator+'log'+DirectorySeparator;
dir:='c:\tmpArea\';
if DirectoryExists(dir)=false then  mkdir(dir);
 err.Lines.SaveToFile(dir+'err.txt');
 logm.Lines.SaveToFile(dir+'log.txt');
 k.Lines.SaveToFile(dir+'Importati.txt');

end;

(*
procedure TimpOld.BitBtn4Click(Sender: TObject);
begin
  loadImportazione('2');
end;

procedure TimpOld.BitBtn5Click(Sender: TObject);
begin
  loadImportazione('3');
end;

procedure TimpOld.BitBtn6Click(Sender: TObject);
begin
  loadImportazione('5');

end;


procedure TimpOld.BitBtn7Click(Sender: TObject);
begin
 loadImportazione('7');
end;

procedure TimpOld.BitBtn8Click(Sender: TObject);
begin
loadImportazione('6');
end;

procedure TimpOld.BitBtn2Click(Sender: TObject);
begin
 loadImportazione('6');
end;

procedure TimpOld.BitBtn3Click(Sender: TObject);
begin
  loadImportazione('1');
end;
  *)
end.



end.


