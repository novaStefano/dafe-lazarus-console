unit import;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,CustApp,
    ZDataset,ZConnection, LazFileUtils,fileUtil,inifiles;

type

  { TimpOld }

  TimpDafe = class(TCustomApplication)

    setIns: TZquery;
    setDir: TZquery;
    setRef: TZquery;
    setApp: TZquery;
    SQLServer:TZConnection;
    procedure addOk(mess:string);
    procedure addErr(mess:string);
    procedure log(mess:string);
    procedure checkDirRoot(dir:string);
    function creaCartella():string;
    function creaCartellaSub(idPadre,nome:string):string;
    procedure checkDIr(dir,id:string);
    function insFile(nome,id:string):boolean;
    function getIdFolderParent(idUtente,idParent,cartella:string):string;

    procedure Button2Click();
    procedure Button1Click();

    procedure savFile();

    procedure loadImportazione(idImp,anno:string);
    function checkDirImp(path:string):string;
    function getCreaIdFolderImp(idUte,path,idPre:string):string;
   function  getIdFolderImp(idUte:string):string;
   procedure checkFileImp(idUte,cartella,idFolder:string);
   function removeCar(app:string):string;
   function query(var Squery: TZquery; Stringa: String):Boolean;

   procedure showmessage( mess:String);
   function convDir(app:string):string;
   procedure start();
   function ServerConnect():Boolean;

  private
    { private declarations }
  public
   insSottoValore:string; //gestione delle sotto azienda
    { public declarations }
  end;

var
  impDafe: TimpDafe;
  path,pathStore:string;



 implementation




function TimpDafe.removeCar(app:string):string;
begin
  (*
  app:=StringReplace(app,'à','a',[rfReplaceAll]);
  app:=StringReplace(app,'ò','o',[rfReplaceAll]);
  app:=StringReplace(app,'ì','i',[rfReplaceAll]);
  app:=StringReplace(app,'ù','u',[rfReplaceAll]);
  app:=StringReplace(app,'è','e',[rfReplaceAll]);
  *)

  app:=StringReplace(app,'/','',[rfReplaceAll]);
  app:=StringReplace(app,'\','',[rfReplaceAll]);
  app:=StringReplace(app,':','',[rfReplaceAll]);
  app:=StringReplace(app,'*','',[rfReplaceAll]);
  app:=StringReplace(app,'?','',[rfReplaceAll]);
  app:=StringReplace(app,'<','',[rfReplaceAll]);
  app:=StringReplace(app,'>','',[rfReplaceAll]);
  app:=StringReplace(app,'|','',[rfReplaceAll]);
  removeCar:=app;
end;

function TimpDafe.convDir(app:string):string;
begin
    convDir:=StringReplace(app,'~',DirectorySeparator,[rfReplaceAll]);
end;




function TimpDafe.query(var Squery: TZquery; Stringa: String):Boolean;
var
  infine: Boolean;
begin
  infine:=True;
  Squery.Close;
  Squery.SQL.Clear;
  Squery.SQL.add(Stringa);
  try
       Squery.Open;
       infine:=true;
  except on e:exception do
    if e.Message <> 'Can not open a Resultset' then
         begin
        try
             writeln(e.message);
             writeln(stringa);
            //     Error.Close;
          //       Error.sql.Clear;
        //        Error.sql.add(' insert into errore(descrizione,errore,title,indice)values("'+api(Stringa)+'","'+api(e.message)+'","'+api(application.Title)+'","0") ');
        //    Error.ExecSQL;
        except
          infine:=false;
               // showmessage('Impossibile raggiungere il database, provare a riavviare il programma per risolvere il problema');
        end;
        infine:=False;
          end
    else infine:=True;
  end;
  query:=infine;
end;


function api(app: String): string;   //funzione per apici
var
  a, stringa : String;
  k,i: Integer;
begin

        a:='';
	for i:=1 to length(app) do
  	if app[i]='\' then a:=a+'\'+app[i]
    	else a:=a+app[i];
	app:=a;

	a:='';
	for i:=1 to length(app) do
  	if app[i]='''' then a:=a+'\'+app[i]
    	else a:=a+app[i];
	app:=a;

        a:='';
	for i:=1 to length(app) do
  	if app[i]='"' then a:=a+'\'+app[i]
    	else a:=a+app[i];
	app:=a;
  api := app;
end;


procedure timpDafe.showmessage(mess:string);
begin
  writeln(mess+'\n');
end;


function TimpDafe.creaCartella():string;
var percorso,ragione,idUtente:string;
begin
ragione:=setDir.fieldbyname('ragioneSociale').asstring;
            idUtente:=setDir.fieldbyname('idUtente').asstring;
         percorso:=(ragione)+'-'+idUtente+'/';
         percorso:= removeCar(percorso);


     query(setApp,' select idCartella from cartella where idUtente="'+idUtente+'" and cartellaPadre is null ');   //cerco la cartella root dell'utente
     if setApp.RecordCount=1 then
        begin
          creaCartella:=setApp.fields[0].asstring;
          exit;
        end;

                 query(setIns,' insert ignore cartella (nome,percorso,idUtente,dataInserimento) '+
                 ' values("'+ragione+' '+idUtente+'","'+api(percorso)+'",'+idUtente+',now()) ');

                 try
                 mkdir(path+ StringReplace(percorso,'/',DirectorySeparator,[]));
                 except
                       on e:exception do
                          begin
                          showmessage(e.Message);

                          end;
                 end;

     setApp.Refresh;
     creaCartella:=setApp.fields[0].asstring;

end;


function TimpDafe.creaCartellaSub(idPadre,nome:string):string;
var percorso,idUtente:string;
begin
     log('Ricerca cartella per ID:'+nome);
     idUtente:=setDir.fieldbyname('idUtente').asstring;
     query(setApp,' select percorso from cartella where idUtente="'+idUtente+'" and idCartella="'+idPadre+'" ');   //cerco la cartella root dell'utente
     percorso:=(setApp.FieldByName('percorso').asstring+nome)+'/';

     percorso:=removeCar(percorso);

     log('Percorso '+percorso);


     query(setApp,' select idCartella from cartella where idUtente="'+idUtente+'" and percorso="'+api(percorso)+'" ');   //cerco la cartella root dell'utente

     if setApp.RecordCount=1 then
        begin
          creaCartellaSub:=setApp.fields[0].asstring;
          exit;
        end;

                 query(setIns,' insert ignore cartella (nome,percorso,idUtente,dataInserimento,cartellaPAdre) '+
                 ' values("'+nome+'","'+api(percorso)+'",'+idUtente+',now(),"'+idPadre+'" ) ');

                 try
                 if DirectoryExists(path+ convDir(percorso))=false then
                 mkdir(path+ convDir(percorso));
                 except
                       on e:exception do
                          begin
                          showmessage(e.Message);

                          end;
                 end;

     setApp.Refresh;
     creaCartellaSub:=setApp.fields[0].asstring;

end;



//inserisce un file processato
function TimpDafe.insFile(nome,id:string):boolean;
var appFile,newFile,strquery:string;
  size:Integer;
  var fi:file;
begin

     // nome:=inizio.removeCar(nome);
     log('Trovato file '+nome);


     appFile:=(ExtractFileName(nome));


     size:=FileUtil.FileSize(nome);

          if(insSottoValore<>'') then  //gestisco la sotto azienda
                            begin
                              appFile:=ExtractFileNameWithoutExt(appFile);
                              appFile:=appFile+'-'+insSottoValore+ ExtractFileExt(nome);
                            end;


     strquery:= ' insert ignore file(nome,idCartella,dataInserimento,size)values("'+api(appFile)+'","'+id+'",now(),"'+IntToStr(size)+'") ';
     query(setApp,strquery);

     query(setApp,' select percorso from cartella where idCartella="'+id+'" ');

     try
     showmessage('prova ricorsiva '+pathStore+convDir(setApp.Fields[0].asstring));
     ForceDirectories(pathStore+convDir(setApp.Fields[0].asstring));
     except
           on e:exception do
              begin
                  showmessage('----> '+e.message);
              end;
     end;


     newFile:=pathStore+convDir(setApp.Fields[0].asstring)+appFile;

     try
     if FileExists(newFile) then
        begin
             log('Elimino il file vecchio');
             try
             DeleteFile(newFile);


             Except on e:Exception do
              log('Errore eliminazione vecchio file '+e.Message);

             end;

        end;

     showmessage('****INIZIO COPIA*****');
     showmessage('*'+nome+'*****');
     showmessage('*'+newFile+'*');



     if CopyFile(nome,newFile ,false) then
        begin
            log('copia file avvenuto con successo');
            log(newFile);
             insFile:=true;
             exit;

        end
        else
            begin
                 addErr('*****************************Errore file!');
                 log('*****************************Errore file!');
            end;



     except on e:exception do
            begin
           addOk('!!!!!!!!!!!!!!!!!!!!!!!!!!!errore copia file '+e.message+'  '+nome+'   '+newFile);
           log('!!!!!!!!!!!!!!!!!!!!!!!!!!!errore copia file '+e.message+'  '+nome+'   '+newFile);

            end;
     end;
     insFile:=false;

end;



procedure TimpDafe.checkDIr(dir,id:string);
var fil:TstringLIST;
  i:integer;
  appFile,appId:string;

begin

     log('Inizio processo cartella '+dir );
fil:=TStringList.Create;
fil:=FindAllDirectories(dir,false);
for i:=0 to fil.Count-1 do  //ricorsione per file
    begin
         appId:=creaCartellaSub(id,ExtractFileName(fil.strings[i]));
         checkDir(fil.Strings[i],appId);
    end;


//inizio caricamento file
fil:=FindAllFiles(dir,'*.*',false);
log('inizio ricerca file '+dir+' trovati '+inttostr(fil.count));
for i:=0 to fil.Count-1 do  //ricorsione per file
    begin
         appFile:=(dir+DirectorySeparator+ExtractFileName(fil.strings[i]));
         insFile(appFile,id);
    end;




end;

procedure TimpDafe.checkDirRoot(dir:string);
var dirName:string;
    idRoot:string;

    app:string;
    ind:integer;
begin
 dirName:=ExtractFileName(dir);


query(setDir,' select * from utente where nota="'+dirName+'" ');
     if setDir.RecordCount=0 then
        begin
             ind:=Pos('_',dirname);
             app:=Copy(dirname,1,ind-1);
             query(setDir,' select * from utente where idReferente="'+app+'" ');
        end;

if setDir.RecordCount=0 then
   begin
     addErr('Errore '+dir+' non trovata nel db');
   end
else
    begin
      addOk('Cartella identificata con l''utente '+setDir.fieldbyname('idUtente').asstring+' '+setDir.FieldByName('ragioneSociale').asstring+' ');
      idRoot:=creaCartella();
      checkDir(dir,idRoot);

    end;


end;

procedure TimpDafe.addOk(mess:string);
begin
writeln('OK-->'+mess+'\n');
end;


procedure TimpDafe.log(mess:string);
begin
writeln('log-->'+mess+'\n');
end;

procedure TimpDafe.addErr(mess:string);
begin
writeln('err-->'+mess+'\n');

end;

procedure TimpDafe.Button2Click();
var str:string;  ragione,appId:string; percorso:string;
begin

showmessage('inizio creazione cartelle utenti nuovi');
  str:=' select * from utente left join cartella on utente.idUtente=cartella.idUtente '+
  ' and cartella.cartellaPadre is null where cartella.idCartella is null ';
  query(setApp,str);

  while not setApp.EOF do
        begin

                      appId:=setApp.fieldbyname('idUtente').asstring;
                      ragione:=setApp.fieldbyname('ragioneSociale').asstring;
                      percorso:=ragione+'-'+setApp.fieldbyname('idUtente').AsString+'/';
                      query(setIns,' insert into cartella (nome,percorso,idUtente,dataInserimento) '+
                      ' values("'+ragione+' '+appId+'","'+percorso+'",'+appId+',now()) ');

                      try
                      mkdir(path+ StringReplace(percorso,'/',DirectorySeparator,[]));
                      except
                      end;
          setApp.Next;

        end;


end;


procedure TimpDafe.Button1Click();
var cart:string;
fil:TStringList;
i:integer;

appCart:string;
begin
query(setRef,' select valore from impostazioni where nome="path_old" ');
cart:=setRef.FieldByName('valore').asstring;
cart:=convDir(cart);
addOk('Caricamento '+cart);

fil:=TStringList.Create;
fil:=FindAllDirectories(cart,false);
addOk('Trovati '+inttostr(fil.Count));

//start:=strtoint(inputbox('','','0'));
//for i:=start To fil.Count-1 do
  //  begin

      addOk('**********Processo '+inttostr(i));
      checkDirRoot(fil.Strings[i]);
    // end;

end;

function TimpDafe.checkDirImp(path:string):string;
var rep:string;
appPath:string;
id:string;
begin
     insSottoValore:='';    //variabile globale per il controllo del sotto utente
     rep:=setRef.FieldByName('tracciato').asstring;
     appPath:=ExtractFileName(path);
     id:=StringReplace(appPath,rep,'',[]);
     log('Path processata da '+rep+' '+id);
     try     //controllo la validità dell'indice                                                                     |
     StrToInt(id);
     log('Controllo valido');
     except
           log('id non valido '+id);
           addErr(id+' nel formato non valido ');
           checkDirImp:='';
           Exit;
     end;

     query(setapp,'select idUtente,ragioneSociale,if(idReferente like "%|%","'+id+'","") as sottoAzienda from utente_portale where idReferente='+id+' or idReferente like "%|'+id+'|%" ');
     log('query eseguita');
     if setapp.RecordCount=1 then
                             begin

                                  addOk('Contatto trovato '+setApp.FieldByName('idUtente').asstring+' '+setApp.FieldByName('ragioneSociale').asstring+' sotto azienda:'+setApp.FieldByName('sottoAzienda').asstring);
                                  log('Marchio per la notifica mail '+setApp.FieldByName('idUtente').asstring);
                                  query(setins,'insert ignore mail_coda(idUtente,stato,tipo) '+
                                  ' values("'+ setApp.FieldByName('idUtente').asstring+'","0","nuovoFile")');
                                  insSottoValore:=setApp.FieldByName('sottoAzienda').asstring;
                                  checkDirImp:= setApp.FieldByName('idUtente').asstring;


                             end
                                else
                                    begin
                                      addErr('Contatto non trovato '+path);
                                      checkDirImp:='';
                                    end;

end;



function TimpDafe.getCreaIdFolderImp(idUte,path,idPre:string):string;
var app,percorso,newPerc,dir:string;
begin

log('controllo nodo '+path);
if idPre='' then
   begin
        query(setapp,'select idCartella,percorso from cartella where idUtente="'+idUte+'" and cartellaPadre is null ');
        idPre:=setApp.fields[0].asstring;
   end
else    query(setapp,'select idCartella,percorso from cartella where idUtente="'+idUte+'" and idCartella="'+idPre+'" ');

percorso:=setApp.fieldbyname('percorso').asstring;


   newPerc:=(percorso+path);
   //showmessage(newPerc);
   log('percorso '+newPerc);


   query(setapp,'select idCartella from cartella where idUtente="'+idUte+'" and percorso="'+api(newPerc)+'" ');
   if Setapp.recordcount=1 then
      begin  //cartella trovata, esco restituendo l'id
             log('Cartella trovata '+setApp.Fields[0].asstring);
             getCreaIdFolderImp:=setApp.Fields[0].asstring;
             exit;

       end;
   log('Creo la cartella '+path);

  // showmessage('creazione cartella '+newPerc);
   query(setIns,' insert ignore cartella (nome,percorso,idUtente,dataInserimento,cartellaPAdre) '+
                 ' values("'+StringReplace(path,'/','',[rfReplaceAll])+'","'+api(newPerc)+'",'+idUte+',now(),"'+idPre+'" ) ');

                 try
                 dir:=path+ convDir(percorso)+convDir(path);
                 log('Creazione '+dir);
                 if DirectoryExists(dir)=false then


                 mkdir(dir);
                 except
                       on e:exception do
                          begin
                          showmessage(e.Message+'  '+dir);

                          end;
                 end;

     setApp.Refresh;
     getCreaIdFolderImp:=setApp.fields[0].asstring;





end;


//cerca l'id directory dall'idUtente e da una cartella superiore partendo dall'id
function TimpDafe.getIdFolderParent(idUtente,idParent,cartella:string):string;
var str:string;
percorsoAssoluto:string;
begin

//estraggo il nome della cartella da importare
cartella:=(ExtractFileName(cartella));
log('Cartella da cercare:'+cartella+' id cartella padre: '+idParent );

//cerco se la cartella è già esistente
str:=' select idCartella from cartella where idUtente="'+idUtente+'" and nome="'+api(cartella)+'" and cartellaPadre="'+idParent+'" ';
query(setApp,str);


if(setApp.recordcount=1)then
   begin
        getIdFolderParent:=setApp.fields[0].asstring;
   end
      else
          begin    //inserisco il record e restituisco l'id

              query(setApp,'select percorso from cartella as a where a.idCartella="'+idParent+'" ');
              percorsoAssoluto:=setApp.fieldbyname('percorso').asstring;
              percorsoAssoluto:=percorsoAssoluto+cartella+'/';



              str:=' insert into cartella set idUtente="'+idUtente+'",nome="'+api(cartella)+'",cartellaPadre="'+idParent+
               '",percorso="'+api(percorsoAssoluto)+'",dataInserimento=now() ';
               query(setApp,str);
               log(str);

               //riconverto la tilde per il tracciato corretto per creare la directory
               percorsoAssoluto:=path+convDir(percorsoAssoluto);
               log('Creo la directoy '+percorsoAssoluto);
               mkdir(percorsoAssoluto);

               //richiamo la stessa funzione, ora il record c'è
               getIdFolderParent:=getIdFolderParent(idUtente,idParent,cartella);

          end;



end;

//cerco l'id directory e se non c'è la creo
function TimpDafe.getIdFolderImp(idUte:string):string;
var app:string; idApp,appCart,modCart,root:string;
ind:integer;
i:integer;
begin

     log('inizio ricerca indice cartella '+idUte);


     app:=setRef.FieldByName('tracciatoCartella').asstring;
     query(setApp,'select '+app);
     log('select '+app);
     modCart:=api( setApp.fields[0].asstring);
     query(setapp,'select percorso from cartella where idUtente="'+idUte+'" and cartellaPadre is null ');
     root:=setApp.Fields[0].asstring;

     app:=root+modCart;
     log('query -->select idCartella from cartella where idUtente="'+idUte+'" and percorso="'+app+'" ');

     query(setApp,'select idCartella from cartella where idUtente="'+idUte+'" and percorso="'+app+'"');
     if setApp.recordcount=1 then
                                 begin
                                     log('Cartella trovata restituisco l''id '+setApp.fields[0].asstring);
                                     getIdFolderImp:=setApp.fields[0].asstring;
                                     exit;
                                  end;
     log('Cartella non trovata, creo la cartella e il relativo percorso');
      i:=0;
     while (modCart<>'')and(i<30) do   //uscita di sicurezza a 30
           begin
     ind:=pos('/',modCart);
     appCart:=copy(modCart,1,ind);
     modcart:=copy(modCart,ind+1,length(modCart));
     log('**Stringa: '+modCart+' CREO:'+appCart);
     idApp:=getCreaIdFolderImp(idUte,appCart,idApp);
     log('nodo '+modCart+'  '+idApp);
     inc(i);
           end;


       log('Cartella creata restituisco l''id');
       getIdFolderImp:=idApp;


end;


//ricerca nella cartella i file da importare per l'utente - se Id cartella è vuoto viene cercato seguendo il pattern dell'importazione
procedure TimpDafe.checkFileImp(idUte,cartella,idFolder:string);
var fil:tstringlist;
i:integer;
idFolderCiclo:string; //utilizato per il ciclo della ricerca cartella
begin
log('inizio ricerca file cartella '+cartella);
fil:=TStringList.Create;
fil:=FindAllFiles(cartella,'*.*',false);
if (idFolder='') then idFolder:=getIdFolderImp(idUte);  //prima entrata, estraggo/creo la cartella root del tracciato
log(idFolder);

//ciclo per controllare la quantità di file
for i:=0  to fil.Count-1 do
    begin
        if insFile(fil.Strings[i],idFolder) then
         begin
             DeleteFileUTF8(fil.strings[i]);
         end
         else log('Annullo l''eliminazione del file');
    end;



log('inizio ciclo directory da importare');
//ciclo la lista di directory, ricorsivamente richiamo questa routine

fil:=FindAllDirectories(cartella,false);
addOk('Trovati '+inttostr(fil.Count));
for i:=0 To fil.Count-1 do
    begin
        cartella:=fil.Strings[i];
        log('Processo sotto cartella '+cartella);
        //processo anche la sotto cartella
        idFolderCiclo:=getIdFolderParent(idUte,idFolder,cartella);
        checkFileImp(idUte,cartella,idFolderCiclo);
    end;



end;

procedure TimpDafe.loadImportazione(idImp,anno:string);
var cart:string;
fil:tstringList;
i:integer;
idUtente,app:string;
dir:string;
oldP,oldPStore:string;
begin
(*
k.Lines.Text:='';
;
logm.Lines.Text:='';
err.Lines.Text:='';
  *)

//app:=InputBox('Anno','Inserisci l''anno dove inserire i documenti','2015');


query(setRef,' select idImportazione,cartellaSorgente,domanda, replace(tracciatoCartella,"$sost","'+anno+'") as "tracciatoCartella",tracciato '+
' from importazione where idImportazione="'+idImp+'" ');

//if inizio.modi(setRef.FieldByName('domanda').asstring)=false then exit;

cart:=setRef.FieldByName('cartellaSorgente').asstring;
cart:=convDir(cart);
addOk('Caricamento '+cart);

fil:=TStringList.Create;
fil:=FindAllDirectories(cart,false);
addOk('Trovati '+inttostr(fil.Count));
oldP:=path;
oldPStore:=pathStore;
for i:=0 To fil.Count-1 do
    begin
      idUtente:='';
      log('**********Processo '+inttostr(i)+' '+fil.strings[i]);
      idUtente:=checkDirImp(fil.Strings[i]);

      if (idUtente<>'') then
         begin
              path:=oldP+idUtente+DirectorySeparator;
              pathStore:=oldPStore+idUtente;
              log('Path dell''utente '+path);


              query(setIns,'INSERT ignore cartella (nome,percorso,idUtente,dataInserimento) '+
                      ' values("'+DirectorySeparator+'","'+DirectorySeparator+'","'+idUtente+'",now()) ');


              try
              mkdir(path);
              except
              end;

              log('Inizio Importazione utente '+idUtente);
              checkFileImp(idUtente,fil.strings[i],'');


         end;
    end;


showmessage('Ho terminato con successo!');




end;



function timpDafe.ServerConnect():Boolean;
var
  Connesso: Boolean;
  myIni:TIniFile;
  inip:string;
begin
  Connesso := True;
  try
  SQLServer:=TZConnection.create(nil);
  SQLServer.Protocol:='mysql-5';
  ExtractFilePath(ParamStr(0));

//  showmessage('Lettura ini in '+ExtractFilePath(ParamStr(0))+DirectorySeparator+'connection.ini');

    inip:='/home/testLazarus/connection.ini';

  showmessage('Lettura ini in '+inip);

  myIni:=TINIFile.Create(inip);
       SQLServer.HostName := myIni.ReadString('db_Server','host','');
       SQLServer.Port := myIni.ReadInteger('db_Server','porta',3306);
       SQLServer.User := myIni.ReadString('db_Server','user','');
       SQLServer.Password := myIni.ReadString('db_Server','pass','');
       SQLServer.Database := myIni.ReadString('db_Server','schema','');
      // sqlserver.LibraryLocation:=myIni.ReadString('Database','db_lib','');
    SQLServer.Connect;
  except on e:exception do
  begin
    showmessage(e.message);
    Connesso := False;
    end;
  end;
  if Connesso then
    begin


    path:=(myIni.ReadString('path','dir',''));
    pathStore:=(myIni.ReadString('path','dirStore',''));
    //pathSave:=reconvDir(path);

   end
  else
      begin
        writeln('Errore connessione');
        halt();
      end;
  log('connesso');
  ServerConnect := Connesso;
end;




procedure timpDafe.start();
var idLog:string;
begin

     log('Start per dafe');
     ServerConnect();

    log('fine connessione');

    setIns:=TZQuery.Create(nil);
    setIns.Connection:=SQLServer ;

    setDir:=TZQuery.Create(nil);
    setRef:=TZQuery.Create(nil);
    setApp:=TZQuery.Create(nil);

    setDir.Connection:=SQLServer;
    setRef.Connection:=SQLServer ;
    setApp.Connection:=SQLServer ;
    log('assegnazione dataset');

    query(setApp,'SELECT idLog, annoImportazione,idImportazione from cmd_log where eseguita is null and completata is null ');

    if(setApp.RecordCount=0) then
                             begin
                                  showmessage('Nessun avvio impostat');
                                  halt;
                             end;


    idLog:=setApp.FieldByName('idLog').asstring;
    query(setRef,'update cmd_log  set eseguita=now() where idLog='+idLog+' ');
    loadImportazione(setApp.FieldByName('idImportazione').asstring,setApp.FieldByName('annoImportazione').asstring );      //eseguo l'importazione
    query(setRef,'update cmd_log  set completata=now() where idLog='+idLog+' '); //imposto l'importazione esaurita






end;

procedure TimpDafe.savFile();
var dir:string;
begin
(*
dir:=ExtractFileDir(Application.ExeName)+DirectorySeparator+'log'+DirectorySeparator;
dir:='c:\tmpArea\';
if DirectoryExists(dir)=false then  mkdir(dir);
 err.Lines.SaveToFile(dir+'err.txt');
 logm.Lines.SaveToFile(dir+'log.txt');
 k.Lines.SaveToFile(dir+'Importati.txt');
  *)

end;

(*
procedure TimpOld.BitBtn4Click(Sender: TObject);
begin
  loadImportazione('2');
end;

procedure TimpOld.BitBtn5Click(Sender: TObject);
begin
  loadImportazione('3');
end;

procedure TimpOld.BitBtn6Click(Sender: TObject);
begin
  loadImportazione('5');

end;


procedure TimpOld.BitBtn7Click(Sender: TObject);
begin
 loadImportazione('7');
end;

procedure TimpOld.BitBtn8Click(Sender: TObject);
begin
loadImportazione('6');
end;

procedure TimpOld.BitBtn2Click(Sender: TObject);
begin
 loadImportazione('6');
end;

procedure TimpOld.BitBtn3Click(Sender: TObject);
begin
  loadImportazione('1');
end;
  *)
end.

