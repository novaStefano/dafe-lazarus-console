program Project1;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Classes, SysUtils, CustApp, import
  { you can add units after this };

type

  { TDafe }

  TDafe = class(TCustomApplication)
  protected
    procedure DoRun; override;
  public
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
    procedure WriteHelp; virtual;
  end;

{ TDafe }

procedure showmessage(app:string);
begin
  writeln(app);

end;

procedure TDafe.DoRun;
var
  ErrorMsg: String;
  var imp:TimpDafe;
begin
  // quick check parameters
  ErrorMsg:=CheckOptions('h', 'help');
  if ErrorMsg<>'' then begin
    ShowException(Exception.Create(ErrorMsg));
    Terminate;
    Exit;
  end;

  // parse parameters
  if HasOption('h', 'help') then begin
    WriteHelp;
    Terminate;
    Exit;
  end;

  { add your program here }

    writeln('start');


    imp:=TimpDafe.create(nil);
    imp.start();

    writeln('end');

  // stop program loop
  Terminate;
end;

constructor TDafe.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);
  StopOnException:=True;
end;

destructor TDafe.Destroy;
begin
  inherited Destroy;
end;

procedure TDafe.WriteHelp;
begin
  { add your help code here }
  writeln('Usage: ', ExeName, ' -h');
end;

var
  Application: TDafe;
begin
  Application:=TDafe.Create(nil);
  Application.Title:='DafeServirce';
  Application.Run;
  Application.Free;
end.

